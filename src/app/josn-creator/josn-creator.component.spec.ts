import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JosnCreatorComponent } from './josn-creator.component';

describe('JosnCreatorComponent', () => {
  let component: JosnCreatorComponent;
  let fixture: ComponentFixture<JosnCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JosnCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JosnCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
