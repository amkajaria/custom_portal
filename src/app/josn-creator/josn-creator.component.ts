import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { BaseStructure } from '../jsonStructure';

@Component({
  selector: 'app-josn-creator',
  templateUrl: './josn-creator.component.html',
  styleUrls: ['./josn-creator.component.css']
})
export class JosnCreatorComponent implements OnInit {
  parent: string = 'Root';
  value: string = '';
  baseStructure: BaseStructure[] = [];
  dataAdded: boolean = false;

  constructor(private router: Router) {}

  ngOnInit() {
    this.baseStructure.push({ key: this.parent, child: new Array() });
  }

  onEnter(childArr) {
    if (this.value && this.value.length > 0) {
      childArr.forEach((item) => {
        if (item.key === this.parent) {
          item.child.push({ key: this.value, child: new Array() });
          this.dataAdded = true;
        } else {
          this.onEnter(item.child);
        }
      });
    }
    if (this.dataAdded) {
      this.resetData();
    }
  }

  resetData() {
    this.value = '';
    this.parent = 'Root';
    this.dataAdded = false;
  }

  updateParent(newParent) {
    this.parent = newParent;
  }

  onParentChanged(newParent) {
    this.parent = newParent;
  }

  navigateForm() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        baseStructure: JSON.stringify(this.baseStructure)
      },
      skipLocationChange: true
    };
    this.router.navigate(['/form'], navigationExtras);
  }
}
