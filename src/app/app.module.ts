import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { JosnCreatorComponent } from './josn-creator/josn-creator.component';
import { AppRoutingModule } from './app-routing.module';
import { JsonTreeStructureComponent } from './json-tree-structure/json-tree-structure.component';
import { CustomFormComponent } from './custom-form/custom-form.component';

@NgModule({
  declarations: [
    AppComponent,
    JosnCreatorComponent,
    JsonTreeStructureComponent,
    CustomFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
