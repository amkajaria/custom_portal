import { Component, EventEmitter, Output, Input } from '@angular/core';

import { BaseStructure } from '../jsonStructure';

@Component({
  selector: 'app-json-tree-structure',
  templateUrl: './json-tree-structure.component.html',
  styleUrls: ['./json-tree-structure.component.css']
})
export class JsonTreeStructureComponent {
  @Input()
  childList: BaseStructure[];

  @Output()
  parentChanged = new EventEmitter<string>();

  constructor() {}

  updateParent(newParent) {
    this.parentChanged.emit(newParent);
  }
}
