import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonTreeStructureComponent } from './json-tree-structure.component';

describe('JsonTreeStructureComponent', () => {
  let component: JsonTreeStructureComponent;
  let fixture: ComponentFixture<JsonTreeStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonTreeStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonTreeStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
